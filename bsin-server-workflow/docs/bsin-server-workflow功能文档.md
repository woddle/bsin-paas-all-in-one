## bsin-server-workflow接口文档

#### 1、bsin-server-workflow介绍

bsin-server-workflow是基于flowable开发的一款工作流引擎，使用的 BPMN2.0  国际通用的建模语言 。

#### 2、技术架构

​		++  sofaboot

​		++  mybatis

​		++  flowable

#### 3、业务流程

![img.png](../docs/img/微信图片_20221116163920.jpg)

这是工作流的大致流程，bsin-server-workflow主要解决其中流程的启动、任务的流转的复杂逻辑调用

我们尽量减少API的复杂度和个数，把API分类为以下三类比较合适。

1. **流程定义类-负责流程定义的查看**

2. **流程实例类-负责流程实例的查看与操作**

3. **任务实例类-负责任务实例的查看与操作**

   在BPMN2.0模型部署之后，会在flowable的流程定义表act_re_procdef中产生一条流程定义相关的数据，

   根据流程定义id，我们可以启动一个或多个流程实例，相应的我们可以通过BsinRuntimeService 服务对流程实例进行操作。流程实例启动之后，首先会开启节点流程到下一个审批节点，当审批节点为任务节点时，我们可以通过BsinTaskService服务对任务进行相关操作。

#### 4、业务模块

###### 	BsinRuntimeService 

​	——流程实例相关的服务模块

​	接口：

​		1、startProcessInstanceByProcessDefinitionId

​			根据流程定义Id启动流程实例（可以设置变量和流程发起人）

​		2、startProcessInstanceWithForm

​			附带表单数据启动流程实例

​		3、getProcessInstanceById

​			根据流程实例id获取流程实例信息

​		4、suspendProcessInstanceById

​			根据流程实例id挂起流程实例

​		5、getSuspendedInstances

​			获取挂起的流程实例列表

​		6、activateProcessInstanceById

​			根据流程实例id激活流程实例

​		7、getActiveInstances

​			获取激活的流程实例列表

​		8、deleteProcessInstance

​			根据流程实例id删除流程实例

###### 	BsinTaskService

​	——任务实例相关的服务模块

​	接口：

​		1、getTaskByUser

​			根据用户名获取待办任务列表

​		2、transfer

​			任务转办

​		3、delegateTask

​			任务委派

​		4、resolve

​			任务解决

​		5、complete

​			任务完成

​		6、completeTaskWithForm

​			附带表单数据完成任务

​		7、setAssignee

​			设置用户任务受理人

​		8、getAllNode

​			获取流程的所有节点列表(通过流程定义id)

​		9、getCurrentNodeInfo

​			获取当前节点信息

​		10、getNextNodeInfo

​			获取下一节点信息

​		11、getPreviousNodeInfo

​			获取上一节点信息

###### 	BsinFormRepositoryService

​	——表单相关的服务模块

​	接口：

​		1、saveForm

​			保存表单（保存模型）

​		2、getFormList

​			获取表单集合

​		3、getFormInfo

​			获取表单信息

​		4、deployForm

​			部署表单（deploymentId 流程部署id）

​		5、getStartFormData

​			获取开始表单数据

​		6、getTaskFormData

​			获取任务表单数据

​		

##### 	监听器（任务监听器、执行监听器）

在bsin-server-workflow中常用的监听器主要有两种：任务监听器、执行监听器

1、任务监听器

任务监听有三种事件类型

1. create ：在任务创建且所有任务属性设置完成之后才触发。

2.assignment ：在任务被分配给某个班里人之后触发，它是在create事件触发前被触发。

3.complete：在配置了监听器的上一个任务完成是触发，也就是说运行期任务删除之前触发。

任务监听器必须要 实现 org.flowable.engine.delegate.TaskListener 接口，创建监听器 

```
public class UserTaskListene implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        String name = delegateTask.getName();
        System.out.println(name);
        if (name.equals("admin审批")){
            delegateTask.setAssignee("admin10");
        }
    }
}
```

在绘制流程图的时候有三种监听器执行类型 

1、 class：需要类的全路径 

![img.png](../docs/img/class.png)

2、 expression：定义一个表达式，类似EL的语法 

![img.png](../docs/img/expression.png)

3、 delegateExpression:允许您指定一个解析为实现该TaskListener接口的对象的表达式，类似于服务任务 

![img.png](../docs/img/delegateExpression.png) 

##### 	

2、执行监听器

​	 执行监听器允许您在流程执行期间发生特定事件时执行外部Java代码或评估表达式 

 实现一个org.flowable.engine.delegate.ExecutionListener接口 

```
@Component("MyExecutionListener")
public class MyExecutionListener implements ExecutionListener {
    public FixedValue ssss;
    
    /**
     * 执行监听器
     * @param delegateExecution
     */
    @Override
    public void notify(DelegateExecution delegateExecution) {
        // 获取执行监听器中的字段值
        System.out.println(ssss.getExpressionText());
        System.out.println("执行监听器测试成功");
    }
}
```

在绘制流程图的时候有三种监听器执行类型 ，同任务监听器类似。



##### 服务任务的执行

​	 服务任务执行的有三种方式——监听类、委托表达式、表达式

​	1、监听类

​		首先我们可以设置一个监听类，这个监听类有一个硬性规定就是需要实现 JavaDelegate 接口

```
public class MyServiceTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("========MyServiceTask==========");
    }
}
```

在这个监听类中我们可以完成一些操作，通过这个 execution 也可以获取到在流程节点之间传输的变量。

定义好后，在我们画流程的时候，配置这个类的全路径即可，列：org.javaboy.flowableidm.MyServiceTask

![img.png](../docs/img/1.png)

2、委托表达式

委托表达式是指将一个实现了 JavaDelegate 接口的类注册到 Spring 容器中，然后我们在流程节点的配置中不用写完整的类名了，只需要写 Spring 容器中的 Bean 名称即可。

```
@Component
public class MyServiceTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("========MyServiceTask==========");
    }
}
```

定义好后，在我们画流程的时候，配置委托表达式，列：${myServiceTask}

![img.png](../docs/img/2.png) 

3、表达式

表达式就是一个普通类的普通方法，将这个普通类注册到 Spring 容器中，然后表达式中还可以执行这个类中的方法，类似下面这样，任意定义一个 Java 类：

```
@Component
public class MyServiceTask2 {
    public void hello() {
        System.out.println("========MyServiceTask2==========");
    }
}
```

定义好后，在我们画流程的时候，配置表达式，列：${myServiceTask2.hello()}

![img.png](../docs/img/3.png)

4、类中的字段

我们在绘制流程图的时候，还可以为类设置一个字段。

![img.png](../docs/img/4.png)

我们就可以在 Java 类中访问到这个变量了，如下：

```
@Component
public class MyServiceTask {
    FixedValue username;
    
    public void hello() {
        String expressionText = username.getExpressionText();
        System.out.println("========MyServiceTask==========");
        System.out.println(expressionText);
        System.out.println("测试服务任务执行方式————表达式");
    }
}
```

