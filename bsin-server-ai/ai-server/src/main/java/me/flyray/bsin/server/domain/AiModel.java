package me.flyray.bsin.server.domain;

import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * @TableName ai_tenant_ai_model
 */
@Data
public class AiModel {
    private String serialNo;

    private String tenantId;

    /**
     * 模型名称
     */
    private String name;

    /**
     * 模型编号
     */
    private String code;

    /**
     * openAI key
     */
    private String key;

    /**
     * openAI keyList
     */
    private List<String> keyList;

    /**
     * 设置chatGPT url
     */
    private String url;

    /**
     * 模型
     */
    private String model;

    /**
     * 上下文条数
     */
    private Integer contextLimitNum;

    /**
     * 上下文时间
     */
    private Integer contextExpire;

    /**
     * 设置chatGPT maxToken
     */
    private Integer maxToken;

    /**
     * 设置chatGPT temperature
     */
    private Double temperature;

    /**
     * 是否启用模板推送
     */
    private String templateEnable;
    /**
     * 是否启用上下文
     */
    private String contextEnable;

    /**
     * 是否启用代理
     */
    private String proxyEnable;


    /**
     * 计费模式
     */
    private String rechargeable;


    /**
     * 是否systemRole上下文
     */
    private String systemRoleEnable;

    /**
     * 接口异常回复
     */
    private String exceptionResponse;

    /**
     * 回复前置
     */
    private String replyPrefix;

    /**
     * 关注回复
     */
    private String subscribeResponse;


    /**
     * 预回复：为空时啥也不回复
     */
    private String preResponse;

    /**
     * 欠费回复
     */
    private String outofcreditResponse;


    /**
     * 敏感词回复
     */
    private String filterCheckResponse;

    /**
     * 自我介绍词汇
     */
    private String selfIntroductionCopyWriting;

    /**
     * 描述
     */
    private String description;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

}