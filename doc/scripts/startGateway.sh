#!/usr/bin/env bash

nohup java -jar -Xms1024m -Xmx2048m  -XX:PermSize=512M -XX:MaxPermSize=1024m bsin-server-targe-gateway-1.0.0-SNAPSHOT.jar >./logs/gateway.log &
nohup java -jar -Xms1024m -Xmx2048m  -XX:PermSize=512M -XX:MaxPermSize=1024m upms-server-1.0.0-SNAPSHOT.jar >./logs/upms.log &