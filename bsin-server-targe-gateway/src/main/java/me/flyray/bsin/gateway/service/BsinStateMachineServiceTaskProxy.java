package me.flyray.bsin.gateway.service;

import me.flyray.bsin.gateway.common.ApiResult;
import me.flyray.bsin.gateway.domain.BsinChoreographyServiceDo;
import me.flyray.bsin.gateway.pojo.Parameter;

import java.util.Map;

public interface BsinStateMachineServiceTaskProxy {

    Object execute(Map<String,Object> parameter);

    Object execute(Parameter parameter);

    public ApiResult choreographyInvoke(BsinChoreographyServiceDo choreographyServiceBiz, Map<String, Object> reqParam);

}
